<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Auth;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected function redirectTo()
    {
        if (Auth::user()->blocked == "Yes") {
            return '/after_verify';
        } else {
            return '/home';
        }
    }

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    /**
     * Get the login username to be used by the controller.
     *
     * @return string
     */
    public function username()
    {
        Validator::make(request()->input(), [
            'email' => ['sometimes', 'required', 'string', 'email', 'max:25'],
            'phone' => 'sometimes|required|regex:/^[0-9]{10}/|digits:10',
        ])-> validate();

       request()->input('email') != NULL ? $login = request()->input('email') : $login = request()->input('phone');
       $field = filter_var($login, FILTER_VALIDATE_EMAIL) ? 'email' : 'phone';
       request()->merge([$field => $login]);
       return $field;
    }
}
