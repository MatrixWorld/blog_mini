<?php

namespace App\Http\Controllers\Blog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\User;
use Illuminate\Support\Facades\Auth;

class AvatarController extends Controller
{
    /**
     * Create user avatar.
     *
     * @param  array  $data
     * @return \App\Models\User
     */
    protected function upload(Request $request)
    {
        $user = Auth::user();
        $user->clearMediaCollection('avatars');
        if (isset($request['avatar'])) {
            $user->addMedia($request['avatar'])->toMediaCollection('avatars');
        }
        return redirect('home')->with('status', 'Avatar upload successfully.');
    }

    protected function edit()
    {
        $users = User::orderBy('id', 'asc')->paginate(3);
        return view('avatar.edit', compact('users'));
    }

    protected function update($id)
    {
        $user = User::findOrFail($id);
        $user->clearMediaCollection('avatars');
        $user->addMediaFromRequest('avatar')->toMediaCollection('avatars');
        return redirect('home')->with('status', 'Avatar updated successfully.');
    }
}
