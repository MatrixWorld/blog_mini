<?php

namespace App\Http\Controllers\Blog;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Posts;
use App\Models\User;
use Illuminate\Support\Facades\Auth;

class PostsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $auth_user = Auth::user();
        $users = User::all();
        $posts = Posts::where('visible', true)->orderBy('user_id', 'asc')->paginate(3);

        return view('posts.posts', compact('posts'), ['users'=>$users, 'auth_user' => $auth_user]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('posts.postsCreate');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $form = $request->all();

        if ( !isset($form['check_box']) ||  $form['check_box'] != 1) {
            $save_form['visible'] = false;
        }

        $save_form['post_name'] = $form['post_name'];
        $save_form['post'] = $form['post'];
        $save_form['_token'] = $form['_token'];
        $save_form['user_id'] = Auth::user()->id;

        $new_post = Posts::create($save_form);

        return redirect('posts')->with('status', 'Post ' . $save_form['post_name'] . ' created');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $post = Posts::find($id);
        return view('posts.postsEdit', ['post'=>$post]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $post = Posts::find($id);
        $form = $request->all();

        if ( !isset($form['check_box']) ||  $form['check_box'] != 1) {
            $save_form['visible'] = false;
        } else {
            $save_form['visible'] = true;
        }

        $post->update($save_form);
        return redirect()->action('Blog\PostsController@index')->with('status', 'Post ' . $post['post_name'] . ' updated');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $post = Posts::find($id);
        $post->delete();
        return redirect()->action('Blog\PostsController@index')->with('status', 'Post ' . $post['post_name'] . ' deleted');
    }
}
