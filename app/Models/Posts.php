<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'post_name', 'post', 'user_id', 'visible'
    ];

    public function users()
    {
        return $this->belongsTo('App\Models\User', 'id', 'user_id');
    }
}
