<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\Posts;
use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(Posts::class, function (Faker $faker) {
    return [
        // 'post_name' => 'Name_post ' . str_random(5),
        // 'post' => 'Text_post ' . str_random(25),
        'visible' => true,
    ];
});
