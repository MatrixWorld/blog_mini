<?php

use Illuminate\Database\Seeder;

class PostsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for($i=1; $i<5; $i++) {
            factory(App\Models\Posts::class, 5)->create(
                [
                    'post_name' => 'Name_post ' . $i,
                    'post' => 'Text_post ' . $i,
                    'user_id' => $i,
                ]
            );
            // ->each(function ($u) {
            //     $u->posts()->save(factory(App\Models\Posts::class)->make());
            // });
        }
    }
}
