<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(App\Models\User::class, 1)->create(
            [
                'name' => 'User1',
                'email' => 'qqq@qq.qq',
                'email_verified_at' => now(),
                'password' => bcrypt('qwertyui'),
                'remember_token' => Str::random(10),
                'role' => 'Admin',
                'blocked' => 'No',
            ]
        );
        factory(App\Models\User::class, 1)->create(
            [
                'name' => 'User2',
                'email' => 'aaa@aa.aa',
                'email_verified_at' => now(),
                'password' => bcrypt('qwertyui'),
                'remember_token' => Str::random(10),
                'role' => 'User',
                'blocked' => 'No',
            ]
        );
        factory(App\Models\User::class, 1)->create(
            [
                'name' => 'User3',
                'phone' => '7777771234',
                'password' => bcrypt('qwertyui'),
                'remember_token' => Str::random(10),
                'role' => 'User',
                'blocked' => 'No',
            ]
        );
        factory(App\Models\User::class, 1)->create(
            [
                'name' => 'User4',
                'phone' => '9998881234',
                'password' => bcrypt('qwertyui'),
                'remember_token' => Str::random(10),
                'role' => 'User',
                'blocked' => 'No',
            ]
        );
        factory(App\Models\User::class, 1)->create(
            [
                'name' => 'User5',
                'email' => 'zzz@zz.zz',
                'password' => bcrypt('qwertyui'),
                'remember_token' => Str::random(10),
                'role' => 'User',
                'blocked' => 'Yes',
            ]
        );
    }
}
