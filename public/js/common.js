$(document).ready(function() {


	$('#reg_type').on('focusout', function reg_type() {
		let f = $('#reg_type');
		var label = f[0].firstElementChild.innerText
		var rt = $('#reg_type input');
		var val = rt[0].value;
		if(val != '') {
			if(/^[0-9]{10}/.test(val)){
				$(f).replaceWith(`
					<div class="form-group row" id="reg_type">
						<label for="phone" class="col-md-4 col-form-label text-md-right">`+label+`</label>

						<div class="col-md-6">
							<input id="phone" type="text" class="form-control " name="phone" value=`+val+` required autocomplete="phone">
						</div>
					</div>
				`);
				f = $('#reg_type');
				$(f).on('focusout', reg_type);
			} else {
				var pattern = /^([a-z0-9_\.-])+@[a-z0-9-]+\.([a-z]{2,7}\.)?[a-z]{2,7}$/i;
				if(pattern.test(val)){
					$(f).replaceWith(`
    					<div class="form-group row" id="reg_type">
    						<label for="email" class="col-md-4 col-form-label text-md-right">`+label+`</label>

    						<div class="col-md-6">
    							<input id="email" type="email" class="form-control " name="email" value=`+val+` required autocomplete="email">
    						</div>
    					</div>
    				`);
					f = $('#reg_type');
					$(f).on('focusout', reg_type);
				}
			}
		}
    });
});
