@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="row">
                    <div class="col-md-8">
                        <div class="title">
                            <a href="{{ url('/posts/') }}"><button id="btn_posts" class="btn btn-light border-primary float-sm-left" type="button">POSTS</button></a>
                        </div>
                    </div>
                </div>
                @if (Auth::user()->role == 'Admin')
                    @foreach ($users as $user)
                        <div class="row">
                            <div class="col-md-8">
                                <div class="col-md-4">
                                    {{$user->name}}
                                    @if ($user->role == 'Admin')
                                        - Admin
                                    @endif
                                </div>
                                <div class="col-md-4">
                                    @if ($user->getMedia('avatars')->first())
                                        <img src="{{$user->getMedia('avatars')->first()->getUrl('thumb')}}">
                                    @else
                                        NO AVATAR {{$user->name}}
                                    @endif
                                </div>

                                <form id={{$user->id}} method="POST" action="{{ route('update', ['id' => $user->id]) }}" enctype="multipart/form-data">
                                    {{ method_field('DELETE') }}
                                    @csrf
                                    <div class="form-group row">
                                        <label for="avatar" class="col-md-4 col-form-label text-md-right">{{ __('Avatar (optional)') }}</label>

                                        <div class="col-md-8">
                                            <input id="avatar" type="file" class="form-control" name="avatar" required>
                                        </div>
                                    </div>
                                    <div class="form-group row mb-0">
                                        <div class="col-md-6 offset-md-4">
                                            <button id="btn_avatar" type="submit" class="btn btn-primary">Update</button>
                                        </div>
                                    </div>
                                </form>
                                <hr>
                            </div>
                        </div>
                    @endforeach
                    {{ $users->links() }}
                @endif

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
