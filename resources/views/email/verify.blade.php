@component('mail::message')
    # Welcome!

    Thanks for signing up.

    @component('mail::button', ['url' => $url])
    Verify email.
    @endcomponent

    If the button does not work, follow the link {{ $url }}

    Thanks,<br>
    {{ config('app.name') }}
@endcomponent
