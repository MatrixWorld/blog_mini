@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">Dashboard</div>
                <div class="row">
                    <div class="col-md-4">
                        <div class="title">
                            <a href="{{ url('/posts/') }}"><button id="btn_posts" class="btn btn-light border-primary float-sm-left" type="button">POSTS</button></a>
                        </div>
                        <div class="title">
                            <a href="{{ url('/posts/create') }}"><button id="btn_posts_create" class="btn btn-light border-primary float-sm-left" type="button">POST CREATE</button></a>
                        </div>
                    </div>
                    <div class="col-md-8 offset-md-0">
                        <form method="POST" action="{{ route('upload') }}" enctype="multipart/form-data">
                            @csrf
                            <div class="form-group row">
                                <label for="avatar" class="col-md-4 col-form-label text-md-right">{{ __('Avatar (optional)') }}</label>

                                <div class="col-md-8">
                                    <input id="avatar" type="file" class="form-control" name="avatar" required>
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button id="btn_avatar" type="submit" class="btn btn-primary">Upload</button>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="col-md-8">
                        @if (Auth::user()->role == 'Admin')
                            <div class="title">
                                <a href="{{ url('/avatar/edit') }}"><button id="btn_posts_create" class="btn btn-light border-primary float-sm-left" type="button">Edit user avatar</button></a>
                            </div>
                        @endif
                    </div>
                </div>

                <div class="card-body">
                    @if (session('status'))
                        <div class="alert alert-success" role="alert">
                            {{ session('status') }}
                        </div>
                    @endif

                    You are logged in!
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
