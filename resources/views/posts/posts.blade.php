@include('layouts.header')

<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Blog_mini') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ $auth_user->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb font-weight-bold">
                                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Главная</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">User
                                        @if (isset($auth_user))
                                            {{ $auth_user->name }}
                                            @if ($auth_user->role  == 'Admin')
                                                - Admin.
                                            @else
                                                .
                                            @endif
                                        @else
                                            Anonymous
                                        @endif
                                    </li>
                                </ol>
                            </nav>

                            <div class="card-body">
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif

                                <div class="mb-4">
                                    <table class="table table-bordered">
                                        <thead class="thead-dark">
                                            <tr>
                                              <th scope="row">Posts</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            @foreach ($posts as $post)
                                                @if ($post['visible'] == true || (isset($auth_user)))
                                                    <tr class="thead-light">
                                                        <th scope="row">{{$post['post_name']}}</th>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">Author -
                                                            @foreach ($users as $user)
                                                                @if ($user->id == $post['user_id'])
                                                                    {{ $user->name }}
                                                                @endif
                                                            @endforeach
                                                        </th>
                                                    </tr>
                                                    <tr>
                                                        <th scope="row">{{$post['post']}}</th>
                                                    </tr>
                                                    @if (isset($auth_user) && ($auth_user->role == 'Admin' || $post['user_id'] == $auth_user->id))
                                                        <tr class="table-info">
                                                            <th scope="row">
                                                                <a href="{{ url('/posts/'.$post['id'].'/edit') }}"><button class="btn btn-light border-primary float-sm-left" type="button">Edit</button></a>
                                                            </th>
                                                        </tr>
                                                    @endif
                                                @endif
                                            @endforeach
                                            {{ $posts->links() }}
                                        </tbody>
                                    </table>
                                </div>
                                <br>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>
    </div>
</body>
</html>
