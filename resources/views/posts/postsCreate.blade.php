@include('layouts.header')

<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Checklist') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            <div class="container">

                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb font-weight-bold">
                                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Creation</li>
                                </ol>
                            </nav>

                            <div class="card-body">
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif

                                <form id="form_post" method="POST" action="{{ action('Blog\PostsController@store') }}"/>

                                    <div class="form-group {{ $errors->has('post_name') ? ' has-error' : '' }}">
                                        <label for="post_name" class="col-md-4 control-label">Post name</label>

                                        <div class="mb-4 font-weight-bold">
                                            <input id="post_name" type="text" class="form-control" name="post_name" required autofocus value="{{ old('post_name') }}">

                                            @if ($errors->has('post_name'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('post_name') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('post') ? ' has-error' : '' }}">
                                        <label for="post" class="col-md-4 control-label">Post text</label>

                                        <div class="mb-4 font-weight-bold">
                                            <textarea id="post" type="text" class="form-control" name="post" required>{{ old('post') }}</textarea>

                                            @if ($errors->has('post'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('post') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <hr>

                                    <div class="form-group {{ $errors->has('check_box') ? ' has-error' : '' }}">
                                        <div class="input-group">
                                            <div class="custom-control custom-checkbox float-sm-left">
                                                <input class="custom-control-input" id="check_box" name="check_box" type="checkbox" value="1" checked>
                                                <label class="custom-control-label" for="check_box">Visible to all</label>
                                            </div>

                                            @if ($errors->has('check_box'))
                                                <span class="help-block">
                                                    <strong>{{ $errors->first('check_box') }}</strong>
                                                </span>
                                            @endif
                                        </div>
                                    </div>
                                    <hr>

                                    <button class="btn btn-light border-primary" id="btn_post" type="submit">Save</button>

                                    @csrf
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>

    </div>
</body>
</html>
