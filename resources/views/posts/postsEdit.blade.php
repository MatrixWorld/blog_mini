@include('layouts.header')

<body>
    <div id="app">
        <nav class="navbar navbar-expand-md navbar-light bg-white shadow-sm">
            <div class="container">
                <a class="navbar-brand" href="{{ url('/') }}">
                    {{ config('app.name', 'Checklist') }}
                </a>
                <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="{{ __('Toggle navigation') }}">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <!-- Left Side Of Navbar -->
                    <ul class="navbar-nav mr-auto">

                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="navbar-nav ml-auto">
                        <!-- Authentication Links -->
                        @guest
                            <li class="nav-item">
                                <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                            </li>
                            @if (Route::has('register'))
                                <li class="nav-item">
                                    <a class="nav-link" href="{{ route('register') }}">{{ __('Register') }}</a>
                                </li>
                            @endif
                        @else
                            <li class="nav-item dropdown">
                                <a id="navbarDropdown" class="nav-link dropdown-toggle" href="#" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false" v-pre>
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <div class="dropdown-menu dropdown-menu-right" aria-labelledby="navbarDropdown">
                                    <a class="dropdown-item" href="{{ route('logout') }}"
                                       onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                        {{ __('Logout') }}
                                    </a>

                                    <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                        @csrf
                                    </form>
                                </div>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        <main class="py-4">
            <div class="container">

                <div class="row justify-content-center">
                    <div class="col-md-8">
                        <div class="card">
                            <nav aria-label="breadcrumb">
                                <ol class="breadcrumb font-weight-bold">
                                    <li class="breadcrumb-item"><a href="{{ route('home') }}">Home</a></li>
                                    <li class="breadcrumb-item active" aria-current="page">Edit</li>
                                </ol>
                            </nav>

                            <div class="card-body">
                                @if (session('status'))
                                    <div class="alert alert-success" role="alert">
                                        {{ session('status') }}
                                    </div>
                                @endif

                                @if (Auth::user()->role == 'Admin' || $post['user_id'] == Auth::user()->id)
                                    <form id="form_post" method="POST" action="{{ action('Blog\PostsController@update', ['post'=>$post['id']]) }}"/>
                                        {{ method_field('PUT') }}

                                        <div class="form-group">
                                            <p>Post name - {{ $post->post_name }}</p>
                                        </div>
                                        <hr>

                                        <div class="form-group">Post text</div>
                                        <div class="form-group">{{ $post->post }}</div>
                                        <hr>

                                        <div class="form-group {{ $errors->has('check_box') ? ' has-error' : '' }}">
                                            <div class="input-group">
                                                <div class="custom-control custom-checkbox float-sm-left">
                                                    <input class="custom-control-input" id="check_box" name="check_box" type="checkbox" value="1" checked>
                                                    <label class="custom-control-label" for="check_box">Visible to all</label>
                                                </div>

                                                @if ($errors->has('check_box'))
                                                    <span class="help-block">
                                                        <strong>{{ $errors->first('check_box') }}</strong>
                                                    </span>
                                                @endif
                                            </div>
                                        </div>
                                        <hr>

                                        <button class="btn btn-light border-primary" id="btn_post" type="submit">Update</button>
                                        <br>
                                        @csrf

                                    </form>
                                    <br>
                                    <form id="delete_post" method="POST" action="{{ route('destroy', [ 'id'=>$post['id']]) }}">
                                        {{ method_field('DELETE') }}
                                        @csrf
                                        <a href=""><button class="btn btn-light border-primary ml-sm-1" id="del_post" type="submit">Delete</button></a>
                                    </form>
                                @endif
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </main>

    </div>
</body>
</html>
