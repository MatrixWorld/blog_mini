<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes([
    'verify' => true, //email required
    'reset' => false
]);

Route::get('/after_verify', 'HomeController@after_verify')->name('after_verify')->middleware('verified');

Route::resource('posts', 'Blog\PostsController', ['only' => 'index']);
Route::get('/posts', 'Blog\PostsController@index')->name('posts');

Route::group(['middleware' => [ 'auth' ]], function() {
    Route::get('/home', 'HomeController@index')->name('home');

    Route::resource('posts', 'Blog\PostsController', ['only' => 'create', 'store', 'edit', 'destroy']);

    Route::get('posts/create', 'Blog\PostsController@create')->name('create');
    Route::post('posts/store', 'Blog\PostsController@store')->name('store');

    Route::get('posts/{post}/edit','Blog\PostsController@edit');
    Route::post('posts/{post}','Blog\PostsController@update');
    Route::put('posts/{post}','Blog\PostsController@update');
    Route::delete('posts/{post}', 'Blog\PostsController@destroy')->name('destroy');

    Route::post('avatar', 'Blog\AvatarController@upload')->name('upload');
    Route::get('avatar/edit', 'Blog\AvatarController@edit')->name('edit');
    Route::delete('avatar/{id}/update', 'Blog\AvatarController@update')->name('update');
});
