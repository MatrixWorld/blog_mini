<?php

namespace Tests\Feature;

use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
// use Illuminate\Foundation\Testing\RefreshDatabase;
// use Illuminate\Foundation\Testing\WithoutMiddleware;
// use Illuminate\Foundation\Testing\DatabaseMigrations;
use App\Models\User;
use App\Models\Posts;
use Illuminate\Support\Facades\Auth;

use Illuminate\Auth\Events\Registered;
use Illuminate\Auth\Listeners\SendEmailVerificationNotification;

use Illuminate\Http\UploadedFile;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testNoAuthViewPosts()
    {
        /*
        *  Неавторизованным пользователем смотрим посты
        */
        $response = $this->get('/');
        $response->assertStatus(200);
        $response->assertSeeText('Laravel');
        $response->assertSeeText('POSTS');

        $response = $this->get('/posts');
        $response->assertStatus(200);
        $response->assertViewIs('posts.posts');

        $users = User::all();
        $posts = Posts::where('visible', true)->orderBy('user_id', 'asc')->paginate(3);
        $response->assertViewHas(['auth_user' => null, 'posts' => $posts, 'users' => $users]);
        $response->viewData('posts');
        $response->assertSeeText('Posts');
        $response->assertSeeText('Author -')->assertSee('User1');
        $response->assertSeeInOrder(['Name_post 1','User1','Text_post 1']);
        $response->assertDontSeeText('Edit');
        $response->assertSeeText('Главная');
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function NetestAuthNoAdmin()
    {
        /*
        *  Авторизованный User2 смотрит посты
        *  && php7.3 artisan migrate:refresh --seed
        */
        $user = User::findOrFail($id=2);

        // $response = $this->actingAs($user, 'web')
        //                     ->withSession(['banned' => false])
        //                     ->get('/login');
        // $response->assertStatus(302);
        // $response->assertRedirect('/home');

        $response = $this->actingAs($user, 'web')->get('/home');
        $response->assertStatus(200);
        $response->assertViewIs('home');
        $response->assertSeeText('You are logged in!');
        $response->assertDontSeeText('Edit user avatar');
        // $response->assertSeeText('Browse...');
        $response->assertSeeText('POSTS');

        $response = $this->get('/posts');
        $response->assertStatus(200);
        $response->assertViewIs('posts.posts');
        $response->assertSeeText('Author -')->assertSee('User1')->assertSeeInOrder(['Name_post 1','User1','Text_post 1']);
        $response->assertSeeText('2');

        $response = $this->get('/posts?page=2');
        $response->assertStatus(200)->assertViewIs('posts.posts');
        $response->assertSeeText('Author -')->assertSee('User2')->assertSeeInOrder(['Name_post 2','User2','Text_post 2'])->assertSeeText('Edit');

        /*
        *  Авторизованный User2 редактирует свой пост id=10, visible=false
        */
        $response = $this->get('/posts/10/edit');
        $response->assertStatus(200);
        $response->assertViewIs('posts.postsEdit');
        $response->assertSeeText('Post name -')->assertSee('Name_post 2')->assertSeeText('Visible to all')->assertSeeText('Update')->assertSeeText('Delete');

        $content = $response->getContent();
        preg_match_all('/_token" value="([\\S]{40})"/', $content, $matches);

        $response = $this->call('PUT', '/posts/10', ['_token' => $matches[1][0], 'check_box' => 0]);
        $response->assertStatus(302);
        $response->assertRedirect('/posts');

        $response = $this->get('/posts');
        $response->assertStatus(200);
        $response->assertViewIs('posts.posts');
        $response->assertSeeText('Post Name_post 2 updated');

        /*
        *  Авторизованный User2 разлогинивается
        */
        $content = $response->getContent();
        preg_match_all('/_token" value="([\\S]{40})"/', $content, $matches);

        $response = $this->call('POST', '/logout', ['_token' => $matches[1][0]]);
        $response->assertStatus(302);
        $response->assertRedirect('/');

        /*
        *  Неавторизованным пользователем смотрим изменения
        */
        $response = $this->get('/posts?page=2');
        $response->assertStatus(200);
        $response->assertViewIs('posts.posts');
        // $response->assertDontSeeText('Name_post 2')->assertDontSeeText('User2')->assertDontSee('Text_post 2');
        /*
        *  Названия и тексты постов одинаковые. Поэтому текст выше будет виден от другого поста.
        */

        /*
        *  Логин User2
        */
        $response = $this->get('/login');
        $content = $response->getContent();
        preg_match_all('/_token" value="([\\S]{40})"/', $content, $matches);

        $response = $this->call('POST', '/login', ['_token' => $matches[1][0], 'email' => 'aaa@aa.aa', 'password' => 'qwertyui']);
        $response->assertStatus(302);
        $response->assertRedirect('/home');

        /*
        *  Авторизованный User2 редактирует свой пост id=10, visible=true
        */
        $response = $this->get('/posts/10/edit');
        $response->assertStatus(200);

        $content = $response->getContent();
        preg_match_all('/_token" value="([\\S]{40})"/', $content, $matches);

        $response = $this->call('PUT', '/posts/10', ['_token' => $matches[1][0], 'check_box' => 1]);
        $response->assertStatus(302);
        $response->assertRedirect('/posts');

        $response = $this->get('/posts');
        $response->assertStatus(200);
        $response->assertViewIs('posts.posts');
        $response->assertSeeText('Post Name_post 2 updated');

        /*
        *  Авторизованный User2 создает новый пост
        */
        $response = $this->get('/posts/create');
        $response->assertStatus(200);
        $response->assertViewIs('posts.postsCreate');
        $response->assertSeeText('Save');

        $content = $response->getContent();
        preg_match_all('/_token" value="([\\S]{40})"/', $content, $matches);

        $response = $this->call('POST', '/posts/store', ['_token' => $matches[1][0], 'check_box' => 1, 'post_name' => 'Name_post AAAAA', 'post' => 'User2textPOSTnewZZZZZ']);
        $response->assertStatus(302);
        $response->assertRedirect('/posts');
        $response = $this->get('/posts');
        $response->assertStatus(200);
        $response->assertSeeText('Post Name_post AAAAA created');

        /*
        *  Авторизованный User2 смотрит созданный новый пост
        */
        $response = $this->get('/posts?page=4');
        $response->assertStatus(200);
        $response->assertViewIs('posts.posts');
        $response->assertSeeInOrder(['Name_post AAAAA','User2','User2textPOSTnewZZZZZ']);

        /*
        *  Авторизованный User2 удаляет созданный новый пост id=21
        */
        // $response = $this->get('/posts?page=4');
        // $response->assertStatus(200);
        $response = $this->get('/posts/21/edit');
        $response->assertStatus(200);

        $content = $response->getContent();
        preg_match_all('/_token" value="([\\S]{40})"/', $content, $matches);

        $response = $this->call('DELETE', '/posts/21', ['_token' => $matches[1][0], 'id' => 21]);
        $response->assertStatus(302);
        $response->assertRedirect('/posts');

        $response = $this->get('/posts');
        $response->assertStatus(200);
        $response->assertViewIs('posts.posts');
        $response->assertSeeText('Post Name_post AAAAA deleted');

        /*
        *  Авторизованный User2 создает новый пост для Админа id=22 page=4 post=3
        */
        $response = $this->get('/posts/create');
        $response->assertStatus(200);

        $content = $response->getContent();
        preg_match_all('/_token" value="([\\S]{40})"/', $content, $matches);

        $response = $this->call('POST', '/posts/store', ['_token' => $matches[1][0], 'check_box' => 1, 'post_name' => 'Name_post AAAAAforADMIN', 'post' => 'User2textPOSTnewZZZZZ']);
        $response->assertStatus(302);
        $response->assertRedirect('/posts');
        $response = $this->get('/posts');
        $response->assertStatus(200);
        $response->assertSeeText('Post Name_post AAAAAforADMIN created');
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function NetestAuthAdmin()
    {
        $user = User::findOrFail($id=1);

        /*
        *  Авторизованный User1 Admin смотрит посты и удаляет пост User2 id=22 page=4
        *  && php7.3 artisan migrate:refresh --seed
        */
        $response = $this->actingAs($user, 'web')->get('/posts');
        $response->assertStatus(200);
        $response = $this->get('/posts?page=4');
        $response->assertStatus(200);
        $response = $this->get('/posts/22/edit');
        $response->assertStatus(200);

        $content = $response->getContent();
        preg_match_all('/_token" value="([\\S]{40})"/', $content, $matches);

        $response = $this->call('DELETE', '/posts/22', ['_token' => $matches[1][0], 'id' => 22]);
        $response->assertStatus(302);
        $response->assertRedirect('/posts');

        $response = $this->get('/posts');
        $response->assertStatus(200);
        $response->assertViewIs('posts.posts');
        $response->assertSeeText('Post Name_post AAAAAforADMIN deleted');

        /*
        *  Admin создает новый пост для Админа id=23 page=2 и удаляет
        */
        $response = $this->get('/posts/create');
        $response->assertStatus(200);

        $content = $response->getContent();
        preg_match_all('/_token" value="([\\S]{40})"/', $content, $matches);

        $response = $this->call('POST', '/posts/store', ['_token' => $matches[1][0], 'check_box' => 1, 'post_name' => 'Name_post ADMINADMINADMIN', 'post' => 'ADMINtextPOSTnewZZZZZ']);
        $response->assertStatus(302);
        $response->assertRedirect('/posts');
        $response = $this->get('/posts');
        $response->assertStatus(200);
        $response->assertSeeText('Post Name_post ADMINADMINADMIN created');
        $response = $this->get('/posts?page=4');
        $response->assertStatus(200);
        $response->assertDontSeeText('Name_post AAAAAforADMIN');
        $response = $this->get('/posts?page=2');
        $response->assertStatus(200);
        $response->assertSeeText('Name_post ADMINADMINADMIN');
        $response = $this->get('/posts/23/edit');
        $response->assertStatus(200);

        $content = $response->getContent();
        preg_match_all('/_token" value="([\\S]{40})"/', $content, $matches);

        $response = $this->call('DELETE', '/posts/23', ['_token' => $matches[1][0], 'id' => 23]);
        $response->assertStatus(302);
        $response->assertRedirect('/posts');

        $response = $this->get('/posts');
        $response->assertStatus(200);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function NetestRegisterAndLoginPhone()
    {
        /*
        *  Регистрация и авторизация по номеру телефона id=6
        *  && php7.3 artisan migrate:refresh --seed
        */
        $response = $this->get('/register');
        $response->assertStatus(200);

        $content = $response->getContent();
        preg_match_all('/_token" value="([\\S]{40})"/', $content, $matches);

        $response = $this->call('POST', '/register', ['_token' => $matches[1][0], 'name' => 'TESTUSER', 'phone' => '9876543210', 'password' => 'qwertyui', 'password_confirmation' => 'qwertyui']);

        $response->assertStatus(302);
        $response->assertRedirect('/home');

        /*
        *  TESTUSER разлогинивается
        */
        $response = $this->get('/home');
        $content = $response->getContent();
        preg_match_all('/_token" value="([\\S]{40})"/', $content, $matches);

        $response = $this->call('POST', '/logout', ['_token' => $matches[1][0]]);
        $response->assertStatus(302);
        $response->assertRedirect('/');

        /*
        *  TESTUSER логин
        */
        $response = $this->get('/login');
        $response->assertStatus(200);
        $content = $response->getContent();
        preg_match_all('/_token" value="([\\S]{40})"/', $content, $matches);

        $response = $this->call('POST', '/login', ['_token' => $matches[1][0], 'phone' => '9876543210', 'password' => 'qwertyui']);
        $response->assertStatus(302);
        $response->assertRedirect('/home');
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function NetestRegisterEmailSendVerification()
    {
        /*
        *  Регистрация по email, отправка письма верификации на почту
        *  && php7.3 artisan migrate:refresh --seed
        */
        // $this->expectsEvents(Registered::class);
        // $this->doesntExpectEvents(SendEmailVerificationNotification::class);
        // $this->withoutEvents();

        $response = $this->get('/register');
        $response->assertStatus(200);

        $content = $response->getContent();
        preg_match_all('/_token" value="([\\S]{40})"/', $content, $matches);

        $response = $this->call('POST', '/register', ['_token' => $matches[1][0], 'name' => 'TESTUSEREMAIL', 'email' => 'xxx@xx.xx', 'password' => 'qwertyui', 'password_confirmation' => 'qwertyui']);

        $response->assertStatus(302);
        $response->assertRedirect('/after_verify');
        $response = $this->get('/after_verify');
        $response->assertStatus(302);
        $response->assertRedirect('/email/verify');
        $response = $this->get('/email/verify');
        $response->assertStatus(200);
        $response->assertViewIs('auth.verify');
        $response->assertSeeText('Verify Your Email Address');
        $response->assertSeeText('Before proceeding, please check your email for a verification link.')->assertSeeText('If you did not receive the email, click here to request another.');

        /*
        *  Вставляем ссылку верификации из письма
        */
        // $response = $this->get('/email/verify/6?expires=1616110026&hash=279c8d583104f8830796a8ff277e4477a928e9e6&signature=0f86a15030ed84bdc74edecfb87ab920a06610781eca883612362104d62439d3');
        // $response->assertStatus(302);
        // $response->assertRedirect('/login');

        /*
        *  TESTUSER логин
        */
        // $response = $this->get('/login');
        // $response->assertStatus(200);
        // $content = $response->getContent();
        // preg_match_all('/_token" value="([\\S]{40})"/', $content, $matches);
        //
        // $response = $this->call('POST', '/login', ['_token' => $matches[1][0], 'email' => 'xxx@xx.xx', 'password' => 'qwertyui']);
        // $response->assertStatus(302);
        // $response->assertRedirect('/email/verify/6?expires=1616110026&hash=279c8d583104f8830796a8ff277e4477a928e9e6&signature=0f86a15030ed84bdc74edecfb87ab920a06610781eca883612362104d62439d3');
        // $response = $this->get('/email/verify/6?expires=1616110026&hash=279c8d583104f8830796a8ff277e4477a928e9e6&signature=0f86a15030ed84bdc74edecfb87ab920a06610781eca883612362104d62439d3');
        // $response->assertStatus(302);
        // $response->assertRedirect('/home');
        // $response = $this->get('/home');
        // $response->assertStatus(200);
    }

    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function testAvatarUpload()
    {
        /*
        *  Админ загружает User2 аватар.
        */
        $user = User::findOrFail($id=1);

        $response = $this->actingAs($user, 'web')->get('/avatar/edit');
        $response->assertStatus(200);
        $response->assertViewIs('avatar.edit');
        $response->assertSeeText('NO AVATAR User2');

        $content = $response->getContent();
        preg_match_all('/_token" value="([\\S]{40})"/', $content, $matches);

        $stub1 = __DIR__.'/img/avatar1.jpg';
        $avatar = new UploadedFile($stub1, 'avatar1.jpg', filesize($stub1), null, 'image/jpg', true);

        $response = $this->call('DELETE', 'avatar/2/update', ['_token' => $matches[1][0], 'avatar' => $avatar, 'id' => 2], [], ['avatar' => $avatar]);
        $response = $this->get('/avatar/edit');
        $response->assertStatus(200);
        $response->assertDontSeeText('NO AVATAR User2');
        $response->assertSeeText('NO AVATAR User1');
    }

}
